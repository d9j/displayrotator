package com.al.displayrotatator.app;
import android.app.AlertDialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.*;

public class MyAlertDialog extends AlertDialog{


    public MyAlertDialog(Context context, boolean cancelable,
                         OnCancelListener cancelListener,
                         RadioGroup.OnCheckedChangeListener checkedOptionListener,
                         CompoundButton.OnCheckedChangeListener autostartCheckListener
                         ) {
        super(context, cancelable, cancelListener);
        View optionsView = getLayoutInflater().inflate(R.layout.rotation_option_list,null);
        RadioGroup radioGroup = (RadioGroup)optionsView.findViewById(R.id.optionsList);
        CheckBox autostart = (CheckBox)optionsView.findViewById(R.id.autostartBox);


        int chosenRotation = OptionKeys.getSelectedRotation(getContext().getApplicationContext());
        int res_id =  OrientationConverter.stateToResource(chosenRotation);
        int childs = radioGroup.getChildCount();

        Log.d("dialog_get_orientation",String.valueOf(chosenRotation));
        for (int i = 0 ; i < childs-1; i++){
                RadioButton  button =  (RadioButton)radioGroup.getChildAt(i);
                if (button.getId() == res_id) {
                    button.setChecked(true);
                }
        }
        setView(optionsView);
        autostart.setChecked(OptionKeys.orientationAutostart(getContext()));
        autostart.setOnCheckedChangeListener(autostartCheckListener);
        radioGroup.setOnCheckedChangeListener(checkedOptionListener);
        radioGroup.setClickable(true);
    }

}
