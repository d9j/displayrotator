package com.al.displayrotatator.app;


import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import com.al.displayrotatator.app.services.RotationLockerService;

public class RotationServiceManager {

    private Context mContext;

    public RotationServiceManager(Context context){
        mContext = context;
    }
    void  launchRotationLockService(){
        if(!isMyServiceRunning(RotationLockerService.class)){
            Intent i = new Intent(mContext,RotationLockerService.class);
            mContext.startService(i);
        }
    }
    void updateState( int state_id ){
        OptionKeys.setSelectedOption(mContext,state_id);
        Intent i = new Intent(RotationLockerService.STATE_RECEIVER);
        i.putExtra(RotationLockerService.STATE_ID, state_id);
        mContext.sendBroadcast(i);
    }
   public void  StopService(){
        Intent i = new Intent(mContext,RotationLockerService.class);
        mContext.stopService(i);
    }
    public boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        if (manager != null){
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (service.service.getClassName().equals(serviceClass.getName())) {
                    return true;
                }
            }
        }

        return false;
    }

}
