package com.al.displayrotatator.app;


import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.core.app.NotificationCompat;

public class CoreNotification extends NotificationCompat{

    protected final int notif_id = 20;
    private Context mContext;
    private NotificationManager mNotificationManager;

    private final String notifChannel = "com.al.displayrotatator.app.notifchannel";

    public CoreNotification(Context context) {
        this.mContext = context;
    }


    public void buildNotification(String stateName) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            mNotificationManager = mContext.getSystemService(NotificationManager.class);
        }else {
            mNotificationManager =
                    (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        }
        if (mNotificationManager == null){
            return;
        }
        mNotificationManager.cancel(notif_id);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = mContext.getString(R.string.changed_orientation).replace("{}",stateName);
            String description = mContext.getString(R.string.orientation_descr);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(notifChannel, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this

            mNotificationManager.createNotificationChannel(channel);
        }

        mNotificationManager.cancel(notif_id);
        CharSequence name = mContext.getString(R.string.changed_orientation).replace("{}",stateName);
        String description = mContext.getString(R.string.orientation_descr);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(mContext,notifChannel)
                        .setSmallIcon(R.drawable.ic_notif_locker)
                        .setContentTitle(description)
                        .setContentText(name);
        // Creates an explicit intent for an Activity in your app
        Intent resultIntent  = new Intent(mContext, MainActivity.class);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, resultIntent,PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pendingIntent);

        mNotificationManager.notify(notif_id, mBuilder.build());
    }
    public void cancel(){
        if (mNotificationManager != null){
            mNotificationManager.cancel(notif_id);
        }


    }


}
