package com.al.displayrotatator.app.services;

import android.app.*;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.IBinder;
import android.provider.Settings;
import android.widget.Toast;
import com.al.displayrotatator.app.*;

public class RotationLockerService extends Service{

    public final  static  String STATE_RECEIVER = "state_receiver_action";
    public final  static  String STATE_ID = "state_id";
    public final static String DESTROY_SERVICE = "destroy_service";

    CoreNotification coreNotification;
    OrientationManager mOrientationManager;
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public void onCreate() {
        super.onCreate();
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        registerReceiver(mScreenStateReceiver, new IntentFilter(STATE_RECEIVER));
        coreNotification = new CoreNotification(getApplication());
        mOrientationManager = new OrientationManager(getApplication());
        int id = OptionKeys.getSelectedRotation(getApplication());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {

        }else {
            setOrientation(id);
        }


        return super.onStartCommand(intent, flags, START_STICKY_COMPATIBILITY);
    }
    private BroadcastReceiver mScreenStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.hasExtra(STATE_ID)){
                int id = intent.getIntExtra(STATE_ID,0);
                setOrientation(id);
            }
        }
    };
    void  setOrientation(int id){
        String name  = OrientationConverter.stateToString(id,getApplication());
       mOrientationManager.fixOrientation(id);
       coreNotification.buildNotification(name);
       Toast.makeText(getApplicationContext(),getString(R.string.changed_orientation).replace("{}", name),Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean stopService(Intent name) {

        return super.stopService(name);
    }
    @Override
    public void onDestroy() {
        coreNotification.cancel();
        mOrientationManager.restoreView();
        unregisterReceiver(mScreenStateReceiver);
        Toast.makeText(this, "The Application has been stopped!", Toast.LENGTH_LONG).show();
        super.onDestroy();
    }


}
