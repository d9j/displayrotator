package com.al.displayrotatator.app.services;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import com.al.displayrotatator.app.OptionKeys;

public class RotationStartReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
       boolean autostart = OptionKeys.orientationAutostart(context);
        if(autostart){
            Intent i = new Intent(context,RotationLockerService.class);
            context.startService(i);
        }
    }
}
