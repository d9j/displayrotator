package com.al.displayrotatator.app;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.util.Log;

public class OptionKeys {

    public static String OPTIONS_SET = "rotation_options";
    public static String CHOSEN_ROTATION = "chosen_rotation";
    public static String AUTOSTART_ORIENTATION= "auto_start_orientation";
    public static boolean orientationAutostart(Context context){
        return  context.getSharedPreferences(OPTIONS_SET, Context.MODE_PRIVATE).
                getBoolean(AUTOSTART_ORIENTATION,false);
    }

    public static void setAutostart(Context context, boolean start){
    SharedPreferences.Editor editor =    context.getSharedPreferences(OPTIONS_SET, Context.MODE_PRIVATE).edit();
        editor.putBoolean(AUTOSTART_ORIENTATION,start).apply();
    }

    public static void setDefaultOrientation(Context context){
        setSelectedOption(context,ActivityInfo.SCREEN_ORIENTATION_USER);
    }
    /**
     *
     * @param context Activity context
     * @return int Orientation Id
     */
    public static int getSelectedRotation(Context context){
        int id = context.getSharedPreferences(OPTIONS_SET, Context.MODE_PRIVATE).
                getInt(CHOSEN_ROTATION, ActivityInfo.SCREEN_ORIENTATION_USER);
        return  id;
    }
    public static  void setSelectedOption(Context context, int id){
        Log.d("selected_orientation", String.valueOf(id));
      SharedPreferences.Editor editor =   context.getSharedPreferences(OPTIONS_SET, Context.MODE_PRIVATE).edit();
        editor.putInt(CHOSEN_ROTATION,id).apply();
    }
}
