package com.al.displayrotatator.app;


import android.app.Service;
import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

public class OrientationManager {

    private Context mContext;
    private View mView;
    private WindowManager windowManager;

    public OrientationManager(Context context) {
        mContext = context;
        windowManager = (WindowManager) context.getSystemService(Service.WINDOW_SERVICE);
    }
    public  void  fixOrientation(int id){

        WindowManager.LayoutParams layoutParams =  new WindowManager.LayoutParams(
                WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, PixelFormat.OPAQUE
        );;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
             layoutParams = new WindowManager.LayoutParams(
                      WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, PixelFormat.OPAQUE
            );
        }

        layoutParams.screenOrientation = id;
        if(mView == null){
            mView = new View(mContext);
            windowManager.addView(mView,layoutParams);
        }else {
            windowManager.updateViewLayout(mView,layoutParams);
        }
    }
    public void restoreView(){

        WindowManager windowManager = (WindowManager) mContext.getSystemService(Service.WINDOW_SERVICE);
        windowManager.removeViewImmediate(mView);

    }
}
