package com.al.displayrotatator.app;


import android.content.*;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import android.provider.Settings;
import android.widget.*;

public class MainActivity extends FragmentActivity {

   private MyAlertDialog myAlertDialog;
   private RotationServiceManager rotationServiceManager;
    private final  int OVERLAY_PERMISSION =  999;
    CoreNotification coreNotification;
    private boolean testMode = false;
    //  RotationManager rotationManager;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        coreNotification = new CoreNotification(getApplication());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
               Intent permIntent  = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                       Uri.parse("package:" + getPackageName()));
               startActivityForResult(permIntent,OVERLAY_PERMISSION);
        }else {
            setupService();
        }
        myAlertDialog = new MyAlertDialog(this,true,
                cancelListener,checkedOptionListener,autostartCheckListener);
        myAlertDialog.show();
    }

    void  setupService(){
        rotationServiceManager =  new RotationServiceManager(this);
        rotationServiceManager.launchRotationLockService();
    }

    @Override
    protected void onDestroy() {
        if(myAlertDialog.isShowing()){
            myAlertDialog.dismiss();
        }
        super.onDestroy();
    }


    RadioGroup.OnCheckedChangeListener checkedOptionListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
                if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(MainActivity.this) ){
                    requestOverlay();
                    return;
                }
                    RadioButton v =  (RadioButton) group.findViewById(checkedId);
                    if(v.getId() == R.id.stopApp){
                       stopService();
                    }else {
                        Integer state = OrientationConverter.resourceToState(v.getId());
                        rotationServiceManager.updateState(state);
                        //myNotification.buildNotification("State!");


                    }
                    // Finish activity
                    if(!testMode){
                        finish();
                    }


        }

    };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (requestCode == OVERLAY_PERMISSION && Settings.canDrawOverlays(this) ){
                setupService();
            }
        }

    }

    public void requestOverlay(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            Intent permIntent  = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
            startActivityForResult(permIntent,OVERLAY_PERMISSION);
        }



    }

    void  stopService(){
        /** The function setDefaultOrientation shouldn't be here,
         *  but for some reason it's not being saved outside of main activity
         **/
        OptionKeys.setDefaultOrientation(MainActivity.this);
        rotationServiceManager.StopService();
    }

    CompoundButton.OnCheckedChangeListener autostartCheckListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            OptionKeys.setAutostart(MainActivity.this,isChecked);
        }
    };
    DialogInterface.OnCancelListener cancelListener = new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialog) {
            finish();
        }
    };

    public MyAlertDialog getMyAlertDialog() {
        return myAlertDialog;
    }
    public boolean isTestMode() {
        return testMode;
    }

    public void setTestMode(boolean testMode) {
        this.testMode = testMode;
    }
}
