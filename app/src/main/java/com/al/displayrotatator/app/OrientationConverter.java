package com.al.displayrotatator.app;


import android.content.Context;
import android.content.pm.ActivityInfo;

public class OrientationConverter {
    public static String stateToString(int state, Context context){
        String state_name;
        switch (state){
            case ActivityInfo.SCREEN_ORIENTATION_USER:
                state_name =  context.getString(R.string.default_orientation);
                break;
            case  ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE:
                state_name = context.getString(R.string.landscape_orientation);
                break;
            case  ActivityInfo.SCREEN_ORIENTATION_PORTRAIT:
                state_name =  context.getString(R.string.portrait_orientation);
                break;
            case  ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE:
                state_name =  context.getString(R.string.reverse_landscape_orientation);
                break;
            case  ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT:
                state_name = context.getString(R.string.reverse_portrait_orientation);
                break;
            default:
                state_name = context.getString(R.string.default_orientation);
                break;
        }
        return state_name;
    }
    public static int stateToResource(int state){
        int res_id;
        switch (state){
            case ActivityInfo.SCREEN_ORIENTATION_USER:
                res_id = R.id.defaultMode;
                break;
            case  ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE:
                res_id = R.id.landscapeMode;
                break;
            case  ActivityInfo.SCREEN_ORIENTATION_PORTRAIT:
                res_id = R.id.portraitMode;
                break;
            case  ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE:
                res_id = R.id.reverseLandscapeMode;
                break;
            case  ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT:
                res_id = R.id.reversePortraitMode;
                break;
            default:
                res_id = R.id.defaultMode;
                break;
        }
        return res_id;

    }
    public static int resourceToState(int res){
        int state;
        switch (res){
            case R.id.defaultMode:
                state =  ActivityInfo.SCREEN_ORIENTATION_USER;
                break;
            case  R.id.landscapeMode:
                state =  ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                break;
            case  R.id.portraitMode:
                state =  ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                break;
            case  R.id.reverseLandscapeMode:
                state =  ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                break;
            case  R.id.reversePortraitMode:
                state = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                break;
            default:
                state = ActivityInfo.SCREEN_ORIENTATION_USER;
                break;
        }
        return  state;
    }
}
